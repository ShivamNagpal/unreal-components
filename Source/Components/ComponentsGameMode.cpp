// Copyright Epic Games, Inc. All Rights Reserved.

#include "ComponentsGameMode.h"
#include "ComponentsCharacter.h"
#include "UObject/ConstructorHelpers.h"

AComponentsGameMode::AComponentsGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

}
