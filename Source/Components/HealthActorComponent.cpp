// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthActorComponent.h"

#include "Explosion.h"


// Sets default values for this component's properties
UHealthActorComponent::UHealthActorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	this->MaxHealth = 100;
	this->CurrentHealth = this->MaxHealth;
	// ...
}


// Called when the game starts
void UHealthActorComponent::BeginPlay()
{
	Super::BeginPlay();
	this->Owner = GetOwner();
	this->Owner->OnTakeAnyDamage.AddDynamic(this, &UHealthActorComponent::TakeDamageEvent);
}

void UHealthActorComponent::TakeDamageEvent(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
                                            AController* InstigatedBy, AActor* DamageCauser)
{
	CurrentHealth = static_cast<int>(CurrentHealth - Damage + 0.5f);
	CurrentHealth = FMath::Clamp(CurrentHealth, 0, MaxHealth);
	if (CurrentHealth <= 0)
	{
		this->OnHealthDepleted.Broadcast();
		if (UExplosion* ExplosionComponent = this->Owner->FindComponentByClass<UExplosion>(); ExplosionComponent != nullptr)
		{
			ExplosionComponent->Explode_Implementation();
		}
	}
}
