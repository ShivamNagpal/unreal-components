// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthActorComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FHealthDepeletedDelegate);

UCLASS(ClassGroup=(Custom), Blueprintable)
class COMPONENTS_API UHealthActorComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UHealthActorComponent();

	UFUNCTION()
	virtual void TakeDamageEvent(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	                             AController* InstigatedBy, AActor* DamageCauser);

	UPROPERTY(BlueprintAssignable)
	FHealthDepeletedDelegate OnHealthDepleted;

	UPROPERTY(Category=Health, VisibleAnywhere, BlueprintReadOnly)
	int CurrentHealth;

	UPROPERTY(Category=Health, EditAnywhere, BlueprintReadWrite)
	int MaxHealth;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	UPROPERTY()
	AActor* Owner;
};
