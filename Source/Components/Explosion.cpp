// Fill out your copyright notice in the Description page of Project Settings.


#include "Explosion.h"

#include "HealthActorComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"


UExplosion::UExplosion()
{
	PrimaryComponentTick.bCanEverTick = false;
	ExplosionRadius = 500.0f;
	Impulse = 1000000.0f;
}


// Called when the game starts
void UExplosion::BeginPlay()
{
	Super::BeginPlay();
	if (!ExplosionFloatCurve)
	{
		UE_LOG(LogActor, Error, TEXT("Float Curve is not assigned to the ExplosionActorComponent"))
	}
	Owner = this->GetOwner();
}

void UExplosion::Explode_Implementation()
{
	const FTransform ExplosionTransform = this->GetComponentTransform();
	if (NiagaraSystem)
	{
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), NiagaraSystem, ExplosionTransform.GetLocation(),
		                                               ExplosionTransform.GetRotation().Rotator());
	}

	if (UHealthActorComponent* OwnerHealthComponent = Owner->FindComponentByClass<UHealthActorComponent>())
	{
		OwnerHealthComponent->DestroyComponent();
	}

	TArray<TEnumAsByte<EObjectTypeQuery>> traceObjectTypes;
	traceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_PhysicsBody));
	TArray<AActor*> outActors;
	TArray<AActor*> ignoreActors;
	ignoreActors.Add(Owner);
	UKismetSystemLibrary::SphereOverlapActors(GetWorld(), ExplosionTransform.GetLocation(), ExplosionRadius,
	                                          traceObjectTypes, nullptr, ignoreActors, outActors);
	for (AActor* Actor : outActors)
	{
		if (Actor)
		{
			if (const UHealthActorComponent* HealthActorComponent = Actor->FindComponentByClass<
				UHealthActorComponent>())
			{
				const double Distance = FVector::Distance(Actor->GetActorLocation(), ExplosionTransform.GetLocation());
				const double DamageRatio = ExplosionFloatCurve->GetFloatValue(Distance / ExplosionRadius);
				const float Damage = HealthActorComponent->MaxHealth * DamageRatio;
				Actor->TakeDamage(Damage, FDamageEvent(), Owner->GetInstigatorController(), Owner);
			}
			if (UPrimitiveComponent* PrimitiveComponent = Actor->FindComponentByClass<UPrimitiveComponent>())
			{
				if (PrimitiveComponent->IsSimulatingPhysics())
				{
					FVector DirectionVector = UKismetMathLibrary::GetDirectionUnitVector(
						ExplosionTransform.GetLocation(), PrimitiveComponent->GetComponentLocation());
					// DrawDebugDirectionalArrow(GetWorld(), ExplosionTransform.GetLocation(),
					//                           PrimitiveComponent->GetComponentLocation(), 2, FColor::Red, true, 2);
					PrimitiveComponent->
						AddImpulseAtLocation(DirectionVector * Impulse, ExplosionTransform.GetLocation());
				}
			}
		}
	}
	Owner->Destroy();
}
