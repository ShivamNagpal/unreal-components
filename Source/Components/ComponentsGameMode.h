// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ComponentsGameMode.generated.h"

UCLASS(minimalapi)
class AComponentsGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AComponentsGameMode();
};



