// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "Explosion.generated.h"


UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class COMPONENTS_API UExplosion : public USceneComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UExplosion();

	UPROPERTY(Category=Explosion, EditAnywhere, BlueprintReadWrite)
	float ExplosionRadius;

	UPROPERTY(Category=Explosion, EditAnywhere, BlueprintReadWrite)
	float Impulse;

	UPROPERTY(Category=Explosion, EditAnywhere, BlueprintReadWrite)
	UCurveFloat* ExplosionFloatCurve;

	UPROPERTY(Category=Explosion, EditAnywhere, BlueprintReadWrite)
	class UNiagaraSystem* NiagaraSystem;

	UFUNCTION(Category=Explosion, BlueprintNativeEvent, BlueprintCallable)
	void Explode();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	UPROPERTY()
	AActor* Owner;
};
